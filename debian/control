Source: parmed
Section: science
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders:
 Andrius Merkys <merkys@debian.org>,
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 gromacs <!nocheck>,
 gromacs-data <!nocheck>,
 libopenmm-plugins <!nocheck>,
 moreutils,
 python3-dev,
 python3-lxml <!nocheck>,
 python3-networkx <!nocheck>,
 python3-numpy <!nocheck>,
 python3-openmm <!nocheck>,
 python3-pandas <!nocheck>,
 python3-pytest <!nocheck>,
 python3-rdkit <!nocheck>,
 python3-setuptools,
 python3-versioneer,
Testsuite: autopkgtest-pkg-pybuild
Standards-Version: 4.6.1
Homepage: https://parmed.github.io/ParmEd/html/index.html
Vcs-Browser: https://salsa.debian.org/debichem-team/parmed
Vcs-Git: https://salsa.debian.org/debichem-team/parmed.git

Package: python3-parmed
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
X-Python3-Version: ${python3:Versions}
Description: parameter and topology file editor and molecular mechanical simulator
 ParmEd is a package designed to facilitate creating and easily manipulating
 molecular systems that are fully described by a common classical force field.
 Supported force fields include Amber, CHARMM, AMOEBA, and several others that
 share a similar functional form (e.g., GROMOS).
 .
 ParmEd is capable of reading and writing to a wide array of different file
 formats, like the Amber topology and coordinate files, CHARMM PSF, parameter,
 topology, and coordinate files, Tinker parameter, topology, and coordinate
 files, and many others. The expressive central data structure (the 'Structure'
 class) makes it easy to quickly and safely manipulate a chemical system, its
 underlying topology, and force field parameters describing its potential energy
 function.
 .
 There are two parts of ParmEd -- a documented API that one can incorporate into
 their own Python scripts and programs, and a GUI/CLI pair of programs that
 provides a means to quickly perform various modifications to chemical systems
 for rapid prototyping.
 .
 The API also provides bindings to the OpenMM library, permitting one to carry
 out full molecular dynamics investigations using ParmEd on high-performant
 hardware, like AMD and NVidia GPUs.
